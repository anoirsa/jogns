import org.junit.Test;
import validator.ReferenceValidator;

import static org.junit.Assert.assertEquals;

public class TestCases {

    String rNum = ReferenceValidator.referenceNumberFinder(12345);
    @Test
    public void testAssertThatGreater() {
        assertEquals(true,greaterThan(rNum.length()));
    }

    @Test
    public void testAssertThatLess()    {
        assertEquals(true,lessThan(rNum.length()));
    }


    @Test
    public void testAssertThatGrouppedFive() {
        assertEquals(true , grouppedInFive(ReferenceValidator.referenceNumberFinder(12345)));
    }

    @Test
    public void  testAssertNumsAndSpace() {
        assertEquals(true,  NumsAndspaces(ReferenceValidator.referenceNumberFinder(12345)));
    }







    public boolean greaterThan(int given) {
        return  given > 3;
    }

    public boolean lessThan (int given) {
        return given < 20;
    }

    public boolean grouppedInFive(String given) {
        boolean isFive = true;
        for ( int i = 1 ; i < given.length() ; i = i +6) {
           Character charI = given.charAt(i);
           isFive = charI == ' ' ? true : false;

           if (!isFive) {
               break;
           }

        }
        return isFive;
    }

    public boolean NumsAndspaces(String given) {
        boolean charAndNum = true;
        for (int i = 0 ; i< given.length() ;i++) {
            Character charI = given.charAt(i);
            charAndNum = Character.isDigit(charI) || charI ==' ' ? true : false;

            if (!charAndNum) {
                break;
            }
        }
        return  charAndNum;
    }



}
